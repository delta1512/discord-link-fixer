# [Click Here](https://linkfixer.deltadelta.dev/add_to_server) to add the bot to your server


# Supported links

- Twitter - Via [vxtwitter.com](https://github.com/dylanpdx/BetterTwitFix)

- Youtube - Via [yt-dlp](https://github.com/yt-dlp/yt-dlp)

- Tiktok - Via [tiktxk.com](https://github.com/Britmoji/tiktxk)

- Reddit - Via [yt-dlp](https://github.com/yt-dlp/yt-dlp)

- Instagram - Via [ddinstagram.com](https://github.com/Wikidepia/InstaFix)


# How does it work?

1. Post a link that's compatible with the bot (it will react with a 🔧 and a 🗑)

2. Click the 🔧 to fix the link (sometimes this can take a little while because the video has to process)

3. Click the 🗑 to delete the message

- You can also reply to the link you want fixed with `.fix` to also try fixing the link again.

- Links that are downloaded directly (https://linkfixer.deltadelta.dev/ links) will expire at some point based on last access time. So to keep the link active, you need to access the video every now and then.

Here's an example:

![](https://i.imgur.com/b9KOnc4.gif)