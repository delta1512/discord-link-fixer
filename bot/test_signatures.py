import unittest

from bot.link_fixers import TWITTER_SIG, YOUTUBE_SIG, TIKTOK_SIG, REDDIT_SIG, INSTAGRAM_SIG


class TestRegexPatterns(unittest.TestCase):
    def test_twitter_signature(self):
        self.assertIsNotNone(TWITTER_SIG("https://twitter.com/i/status/1703487979732300284"))
        self.assertIsNotNone(TWITTER_SIG("https://x.com/fnaf_funnies/status/1701372397843017807?s=46&t=NBMmHpsGEbcSnCzWPH2-9A"))
        self.assertIsNone(TWITTER_SIG("https://example.com/"))

    def test_youtube_signature(self):
        self.assertIsNotNone(YOUTUBE_SIG("https://youtu.be/rzLIUgnKY40"))
        self.assertIsNotNone(YOUTUBE_SIG("https://m.youtube.com/watch?v=VAh-5ImKXUA"))
        self.assertIsNotNone(YOUTUBE_SIG("https://www.youtube.com/watch?v=oJi3EFLoMmk"))
        self.assertIsNotNone(YOUTUBE_SIG("https://youtube.com/watch?v=oJi3EFLoMmk"))
        self.assertIsNone(YOUTUBE_SIG("https://example.com/"))

    def test_tiktok_signature(self):
        self.assertIsNotNone(TIKTOK_SIG("https://www.tiktok.com/@aussiecarspotting_/video/7264382503409044744?lang=en"))
        self.assertIsNotNone(TIKTOK_SIG("https://vm.tiktok.com/ZSNL3pgys/"))
        self.assertIsNotNone(TIKTOK_SIG("https://vt.tiktok.com/ZSNL3pgys/"))
        self.assertIsNotNone(TIKTOK_SIG("https://tiktok.com/ZSNL3pgys/"))
        self.assertIsNone(TIKTOK_SIG("https://example.com/"))

    def test_reddit_signature(self):
        self.assertIsNotNone(REDDIT_SIG("https://www.reddit.com/r/196/comments/15voc9j/im_bored_so_i_went_back_onto_reddit_for_a_bit/"))
        self.assertIsNotNone(REDDIT_SIG("https://www.reddit.com/r/place/comments/154rtpk/rplace_2023_1_hour_after_start/?utm_source=share&utm_medium=android_app&utm_name=androidcss&utm_term=13&utm_content=1"))
        self.assertIsNotNone(REDDIT_SIG("https://old.reddit.com/r/tf2/comments/14y3sx5/bad_to_the_bone/"))
        self.assertIsNotNone(REDDIT_SIG("https://reddit.com/r/aww/s/80aiNgJYHS"))
        self.assertIsNone(REDDIT_SIG("https://example.com/"))
    
    def test_instagram_signature(self):
        self.assertIsNotNone(INSTAGRAM_SIG("https://www.instagram.com/p/CyHsvzLoNmB/?igshid=MzRlODBiNWFlZA=="))
        self.assertIsNotNone(INSTAGRAM_SIG("https://www.instagram.com/reel/CvNRJoHrwqc/?igshid=MzRlODBiNWFlZA=="))
        self.assertIsNotNone(INSTAGRAM_SIG("https://instagram.com/reel/CvNRJoHrwqc/?igshid=MzRlODBiNWFlZA=="))
        self.assertIsNotNone(INSTAGRAM_SIG("https://instagram.com/p/CyHsvzLoNmB/?igshid=MzRlODBiNWFlZA=="))
        self.assertIsNone(INSTAGRAM_SIG("https://example.com/"))
