import os
import re
import logging
import subprocess as sp

from hashlib import sha1
from dotenv import load_dotenv


load_dotenv()

DL_DIR = os.getenv('DOWNLOAD_DIR')
WEB_HOST_ROOT = os.getenv('WEB_HOST_ROOT')
SOCKS_PROXY_HOST = os.getenv('SOCKS_PROXY_HOST')

TWITTER_SIG = lambda l: re.search(r'https://twitter\.com/|https://x\.com/', l)
YOUTUBE_SIG = lambda l: re.search(r'https://(?:www\.|m.|)youtube\.com/|https://youtu\.be/', l)
TIKTOK_SIG = lambda l: re.search(r'https://(?:www\.|vm\.|vt\.|)tiktok\.com/', l)
REDDIT_SIG = lambda l: re.search(r'https://(?:www\.|old.|)reddit\.com/', l)
INSTAGRAM_SIG = lambda l: re.search(r'https://(www\.|)instagram\.com/', l)
VIDEO_EMBED_HTML = '''
<html>
  <head>
    <title>Discord Link Fixer</title>
    <meta content="Discord Link Fixer" property="og:title" />
    <meta content="{video_url}" property="og:video" />
    <meta content="{video_url}" property="og:video:url" />
    <meta content="{video_url}" property="og:video:secure_url" />
    <meta content="video/mp4" property="og:video:type" />
    <meta property="og:type" content="video.other" />
    <meta content="#43B581" data-react-helmet="true" name="theme-color" />
  </head>
  <body style="background: #212121;">
    <video controls><source src="{video_url}" type="video/mp4"></video>
  </body>
</html>
'''
DEFAULT_YTDL_ARGS = [
    'yt-dlp',
    '--proxy',
    f'socks5://{SOCKS_PROXY_HOST}' if SOCKS_PROXY_HOST else '\"\"'
    '--geo-bypass',
    '--no-playlist',
    '--no-cache-dir',
    '--no-continue',
    '--limit-rate',
    '4M',
    '-f',
    'worst[ext=mp4]' # This seems to work more reliably than checking for a file size
    #'best[width=480][ext=mp4][filesize<30M]/best[ext=mp4][filesize<30M]/worst[ext=mp4][filesize<60M]'
]


def can_fix_link(link: str):
    '''
    :returns: None if can't fix the provided link, else pointer to fixer function
    '''
    for sig in FIXER_MAP:
        if bool(sig(link)):
            return FIXER_MAP[sig]


def create_html_template(file_key: str, video_url: str):
    filled_template = VIDEO_EMBED_HTML.format(video_url=video_url)

    with open(os.path.join(DL_DIR, file_key), 'w') as f:
        f.write(filled_template)


def change_video_format(file_key: str, old_ext: str, new_ext: str):
    '''
    :param old_ext: The old file extension to convert from (with leading .)
    :param new_ext: The new file extension to use (with leading .)
    '''
    old_file = os.path.join(DL_DIR, file_key + old_ext)
    new_file = os.path.join(DL_DIR, file_key + new_ext)
    # Convert using ffmpeg
    sp.run(['ffmpeg', '-i', old_file, new_file], check=False)
    # Remove the old file
    sp.run(['rm', old_file], check=False)
    # Replace all instances of the old ext in the html
    sp.run(
        [
            'sed',
            '-i',
            's/{}/{}/g'.format(old_ext, new_ext).replace('.', ''),
            os.path.join(DL_DIR, file_key)
        ],
        check=False
    )


def fix_link(link: str):
    fixer_func = can_fix_link(link)

    logging.debug('fixer_func %s for link %s', fixer_func, link)

    if fixer_func is None:
        return None

    result = fixer_func(link)

    logging.debug('Result of fixer_func %s', result)

    return result


def fixed_file_exists(file_name: str):
    return os.path.isfile(os.path.join(DL_DIR, file_name))


def fix_twitter_link(link: str):
    if 'x.com' in link:
        return link.replace('x.com', 'vxtwitter.com', 1)
    else:
        return link.replace('twitter.com', 'vxtwitter.com', 1)


def fix_tiktok_link(link: str):
    return link.replace("tiktok.com", "tiktxk.com")


def fix_instagram_link(link: str):
    return link.replace("instagram.com", "ddinstagram.com")


def fix_reddit_link(link: str):
    return yt_download_and_serve_link(
        link,
        sp_args=[
            'yt-dlp',
            '--geo-bypass',
            '--no-cache-dir',
            '--no-continue',
            '--limit-rate',
            '2M',
        ]
    )


def yt_download_and_serve_link(link: str, ytdl_binary: str='yt-dlp', sp_args: list=DEFAULT_YTDL_ARGS):
    file_key = sha1(link.encode()).hexdigest()
    file_name = file_key + '.mp4'
    video_url = WEB_HOST_ROOT + file_name

    ytdl_args = sp_args.copy()

    # Replace the binary
    ytdl_args[0] = ytdl_binary

    # Add the file output
    ytdl_args.insert(1, os.path.join(DL_DIR, file_name))
    ytdl_args.insert(1, '-o')

    # Add the link
    ytdl_args.append(link)

    if not fixed_file_exists(file_name):
        try:
            sp.run(['touch', os.path.join(DL_DIR, file_name)], check=True) # IDK why this is here but we are keeping it
            sp.run(['rm', os.path.join(DL_DIR, file_name)], check=True)
            sp.run(ytdl_args, check=True)
        except sp.CalledProcessError:
            return None

    create_html_template(file_key, video_url)

    return WEB_HOST_ROOT + file_key


FIXER_MAP = {
    TWITTER_SIG : fix_twitter_link,
    YOUTUBE_SIG : yt_download_and_serve_link,
    TIKTOK_SIG  : fix_tiktok_link,
    INSTAGRAM_SIG : fix_instagram_link,
    REDDIT_SIG : fix_reddit_link
}
