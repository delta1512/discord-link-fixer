import os
import re
import sys
import time
import logging
import asyncio

import discord
from discord.ext import commands
from dotenv import load_dotenv

from bot.link_fixers import can_fix_link, fix_link


load_dotenv()

URL_REGEX = r'''(?:https?|ftp):\/\/(?:(?:[^\s()<>]+|\((?:[^\s()<>]+|(?:\([^\s()<>]+\)))?\))+(?:\((?:[^\s()<>]+|(?:\(?:[^\s()<>]+\)))?\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))?'''
WEB_HOST_ROOT = os.getenv('WEB_HOST_ROOT')
PREFIX = '.'

intents = discord.Intents.all()
intents.guild_typing = False #pylint: disable=E0237
intents.bans = False #pylint: disable=E0237
intents.typing = False #pylint: disable=E0237
intents.dm_typing = False #pylint: disable=E0237
intents.invites = False #pylint: disable=E0237
intents.integrations = False #pylint: disable=E0237
intents.members = False #pylint: disable=E0237
intents.presences = False #pylint: disable=E0237
bot = commands.Bot(command_prefix=PREFIX, intents=intents)


def can_fix_message(content: str) -> bool:
    # Find all urls in the content. If there are >1 then ignore
    urls = re.findall(URL_REGEX, content)

    return len(urls) == 1 and can_fix_link(urls[0])


@bot.command()
async def unix(ctx):
    '''A unix time command because I use it too often :P'''
    await ctx.send(str(int(time.time())))


@bot.command()
async def fix(ctx):
    '''Manually fix a link by replying to it'''
    msg = ctx.message

    if msg.reference:
        message_to_fix = await ctx.channel.fetch_message(msg.reference.message_id)
        link_to_fix = message_to_fix.content

        try:
            url = re.findall(URL_REGEX, link_to_fix)[0]
        except IndexError:
            return await msg.channel.send('There is no link in that message')

        fixed_message = fix_link(url)

        if fixed_message is None:
            return await msg.channel.send('Could not fix this link, an unknown error occurred')

        try:
            await msg.channel.send(fixed_message)
        except discord.Forbidden:
            logging.info('Can\'t send message in channel %s', msg.channel)
        except discord.HTTPException:
            logging.error('Failed to send message due to HTTP exception')
    else:
        await msg.channel.send('Reply to the message you want to fix')


@bot.event
async def on_reaction_add(reaction, user):
    if user.bot:
        return

    msg = reaction.message

    # Only delete messages that the bot would have posted and have an expected amount of reactions
    if reaction.count < 2 or not can_fix_message(msg.content):
        return

    if reaction.emoji == '🔧':
        try:
            url = re.findall(URL_REGEX, msg.content)[0]
        except IndexError:
            logging.warning('URL no longer found in the message content')

        await msg.clear_reaction('🔧')

        fixed_message = fix_link(url)

        if fixed_message is None:
            fixed_message = 'Could not fix this link, an unknown error occurred'
            return await msg.channel.send(fixed_message)

        try:
            await msg.channel.send(fixed_message)
        except discord.Forbidden:
            logging.info('Can\'t send message in channel %s', msg.channel)
        except discord.HTTPException:
            logging.error('Failed to send message due to HTTP exception')
    elif reaction.emoji == '🗑️':
        try:
            await msg.delete()
        except (discord.Forbidden, discord.NotFound):
            logging.info('Can\'t delete message, don\'t care')
        except discord.HTTPException:
            logging.warning('Failed to delete the message due to HTTP exception')


@bot.event
async def on_message(msg):
    # Ignore all bots
    if msg.author.bot:
        return

    # Process commands exclusively if message starts with PREFIX
    if msg.content.startswith(PREFIX):
        return await bot.process_commands(msg)

    if not can_fix_message(msg.content):
        return

    # If we can fix the link, add rections
    await msg.add_reaction('🔧')
    await asyncio.sleep(0.25)
    await msg.add_reaction('🗑️')


if __name__ == '__main__':
    discord_key = os.getenv('DISCORD_API_KEY', None)

    if discord_key is None:
        print('No Discord API key was provided. Put one in the .env!')
        logging.error('No Discord API key was provided. Put one in the .env!')
        sys.exit(1)

    bot.run(discord_key)
