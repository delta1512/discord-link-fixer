import os
import logging
from logging.handlers import RotatingFileHandler

from dotenv import load_dotenv

load_dotenv()

logging.basicConfig(
    level=int(os.getenv('LOG_LEVEL', logging.DEBUG)),
    format=os.getenv('LOG_FMT'),
    datefmt=os.getenv('LOG_DATE_FMT')
)
